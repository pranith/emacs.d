;;; -*- no-byte-compile: t -*-
(define-package "rust-mode" "20160721.818" "A major emacs mode for editing Rust source code" '((emacs "24.0")) :url "https://github.com/rust-lang/rust-mode" :keywords '("languages"))
